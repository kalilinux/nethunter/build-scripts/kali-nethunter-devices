---
title: Kali NetHunter Statistics Overview
---

## Images

- [Pre-created Images](/images.html)
- [Device Modules Pre-created Images](/image-models.html)

## Images & Kernels

- [Device Modules Kernels](/device-kernels.html)

## Kernels

- [Kernels](/kernels.html)
- [Android Versions](/android-versions.html)

## Graphs

- [CSV Stats](/csv-stats.html)

- - -

## Links

- [Kali Linux Home](https://www.kali.org/)
- [Kali NetHunter Documentation](https://www.kali.org/docs/nethunter/)
- [Download Kali NetHunter pre-created images](https://www.kali.org/get-kali/#kali-mobile)
- [Kali NetHunter Build-Scripts](https://gitlab.com/kalilinux/nethunter/build-scripts)
- [Kali NetHunter Build-Scripts (Installer)](https://gitlab.com/kalilinux/nethunter/build-scripts/kali-nethunter-installer)
- [Kali NetHunter Build-Scripts (Kernels)](https://gitlab.com/kalilinux/nethunter/build-scripts/kali-nethunter-kernels)
- [Kali NetHunter `devices.yml`](https://gitlab.com/kalilinux/nethunter/build-scripts/kali-nethunter-kernels/-/blob/main/devices.yml)
- [Kali NetHunter Store](https://store.nethunter.com/)
